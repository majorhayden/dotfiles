#!/bin/bash
NVIDIA_OUTPUT=$(nvidia-smi --query-gpu=temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used --format=csv | tail -n1)

IFS=', ' read -r -a array <<< $NVIDIA_OUTPUT
echo "${array[0]}°C ${array[1]}%"
