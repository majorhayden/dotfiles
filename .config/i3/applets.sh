#!/bin/bash

# Kill existing applets
killall -q nm-applet
killall -q pasystray
# killall -q blueberry-tray
killall -q seapplet

# Give the applets a second to exit
sleep 1

# Ensure the applets are dead
while pgrep -u $UID -x nm-applet >/dev/null; do
    sleep 1;
done
while pgrep -u $UID -x pasystray >/dev/null; do
    sleep 1;
done
#while pgrep -u $UID -x blueberry-tray >/dev/null; do
#    sleep 1;
#done
while pgrep -u $UID -x seapplet >/dev/null; do
    sleep 1;
done

# Start the applets
nm-applet --sm-disable &
pasystray -a -m 100 &
#blueberry-tray &
seapplet &
